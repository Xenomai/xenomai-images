#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2018-2023
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

inherit dpkg-raw

DESCRIPTION = "demo image customizations"

SRC_URI = " \
    file://postinst \
    file://primary.network \
    file://99-silent-printk.conf"

DEBIAN_DEPENDS = "openssh-server, systemd-resolved"

do_install() {
	install -d ${D}/etc/systemd/network
	install -m 640 ${WORKDIR}/*.network ${D}/etc/systemd/network/

	install -v -d ${D}/etc/sysctl.d
	install -v -m 644 ${WORKDIR}/99-silent-printk.conf ${D}/etc/sysctl.d/
}
