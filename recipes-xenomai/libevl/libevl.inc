#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2023-2024
#
# Authors:
#  Clara Kowalsky <clara.kowalsky@siemens.com>
#
# SPDX-License-Identifier: MIT
#

inherit dpkg
inherit print-revision

DESCRIPTION ?= "libevl libraries and tools"

SRC_URI += "file://debian"

DEPENDS = "linux-xenomai-4"
DEBIAN_BUILD_DEPENDS ?= "debhelper (>= 10), meson, pkg-config, libbpf-dev"

TEMPLATE_FILES = "debian/control.tmpl"
TEMPLATE_VARS += "DEBIAN_BUILD_DEPENDS"

S = "${WORKDIR}/git"

do_prepare_build[cleandirs] += "${S}/debian"
do_prepare_build() {
    cp -r ${WORKDIR}/debian ${S}
    deb_add_changelog
}
