#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2023-2024
#
# Authors:
#  Clara Kowalsky <clara.kowalsky@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require libevl.inc

SRC_URI += "git://source.denx.de/Xenomai/xenomai4/libevl.git;protocol=https;branch=master;tag=r${PV}"
SRC_URI += "file://0001-meson-setup-uapi-Take-Debian-multiarch-into-account.patch"
