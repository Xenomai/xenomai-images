#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2018-2024
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

inherit dpkg
inherit print-revision

PROVIDES += "xenomai-kernel-source"
PROVIDES += "xenomai-runtime"
PROVIDES += "xenomai-runtime-dbgsym"
PROVIDES += "xenomai-testsuite"
PROVIDES += "xenomai-testsuite-dbgsym"
PROVIDES += "libxenomai1"
PROVIDES += "libxenomai-dev"
PROVIDES += "libxenomai1-dbgsym"

S = "${WORKDIR}/git"

do_prepare_build() {
    # Xenomai currently does not maintain its changelog, plus we want the
    # actual package version in there.
    deb_add_changelog

    # Remove upstream's "3.0 (native)" declaration so that we can define the
    # package version more freely.
    rm -f ${S}/debian/source/format
}
