#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2024
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require xenomai.inc

# Note: When updating PV, also update conf/distro/include/xenomai-version.inc!

SRC_URI = " \
    git://github.com/xenomai-ci/xenomai.git;protocol=https;branch=master;tag=v${PV}"
