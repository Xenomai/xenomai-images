#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2018-2022
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require xenomai.inc

XENOMAI_VERSION = "${@d.getVar('PV').split('-')[1]}"
CHANGELOG_V = "${XENOMAI_VERSION}-head"

SRC_URI = " \
    git://github.com/xenomai-ci/xenomai.git;protocol=https;branch=stable/v${XENOMAI_VERSION}"
SRCREV = "${AUTOREV}"
