#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2018-2023
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require recipes-kernel/linux/linux-custom.inc
inherit print-revision

FILESPATH =. "${LAYERDIR_xenomai}/recipes-kernel/linux/files:"

XENOMAI_DEFCONFIG = "${DISTRO_ARCH}_defconfig"
KERNEL_DEFCONFIG ?= "${XENOMAI_DEFCONFIG}"
XENOMAI_DEBUG ?= "0"

SRC_URI += "${@'file://${KERNEL_DEFCONFIG}' if d.getVar('KERNEL_DEFCONFIG') == d.getVar('XENOMAI_DEFCONFIG') else ''}"
SRC_URI += "${@'file://common_nodbg.cfg' if d.getVar('XENOMAI_DEBUG') == '0' else ''}"

S = "${WORKDIR}/git"
