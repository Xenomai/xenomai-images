#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2021-2023
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require recipes-kernel/linux/linux-xenomai-3-dovetail.inc

SRCREV ?= "v5.10.161-dovetail1"
