#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2018-2023
#
# Authors:
#  Clara Kowalsky <clara.kowalsky@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require recipes-kernel/linux/linux-xenomai.inc

KERNEL_LIBC_DEV_DEPLOY = "1"

SRC_URI += "file://evl.cfg"
