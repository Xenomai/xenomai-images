#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2024
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require recipes-kernel/linux/linux-xenomai-4.inc

PV = "9999-g${COMMIT}"

def is_kernel(d, ver):
    linux_ver = d.getVar('LATEST_GIT_BRANCH') or ''
    return linux_ver == ver

python () {
    import subprocess
    if is_kernel(d, 'latest'):
        cmd = 'git ls-remote --tags --head --refs https://source.denx.de/Xenomai/xenomai4/linux-evl.git | ' \
              'sed -e "s/.*[[:space:]]refs\/\(tags\|heads\)\///" | ' \
              'grep "v6\.[0-9].*-evl-rebase" | ' \
              'sort -r -n -k 1.4 | head -1'
        (ret, out) = subprocess.getstatusoutput(cmd)
        d.setVar('LATEST_KERNEL_BRANCH', out)
    else:
        d.setVar('LATEST_KERNEL_BRANCH', 'v6.6.y-evl-rebase')
}

GIT_BRANCH = "${@ \
    'v6.1.y-evl-rebase' if is_kernel(d, '6.1') \
    else 'v6.6.y-evl-rebase' if is_kernel(d, '6.6') \
    else d.getVar('LATEST_KERNEL_BRANCH') }"

SRC_URI += "git://source.denx.de/Xenomai/xenomai4/linux-evl.git;protocol=https;branch=${GIT_BRANCH}"
SRCREV = "${AUTOREV}"
