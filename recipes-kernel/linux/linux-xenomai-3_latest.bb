#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2019-2023
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require recipes-kernel/linux/linux-xenomai-3.inc

PV = "9999-g${COMMIT}"

def is_xeno_3_0(d):
    xeno_ver = d.getVar('PREFERRED_VERSION_xenomai') or ''
    return xeno_ver.startswith('3.0') or xeno_ver == 'stable-3.0.x'

def is_kernel(d, ver):
    linux_ver = d.getVar('LATEST_GIT_BRANCH') or ''
    return linux_ver == ver

python () {
    import subprocess
    if is_kernel(d, 'latest'):
        cmd = 'git ls-remote --tags --head --refs https://source.denx.de/xenomai/linux-dovetail.git | ' \
              'sed -e "s/.*[[:space:]]refs\/\(tags\|heads\)\///" | ' \
              'grep "v6\.[0-9].*-dovetail-rebase" | ' \
              'sort -r -n -k 1.4 | head -1'
        (ret, out) = subprocess.getstatusoutput(cmd)
        d.setVar('LATEST_KERNEL_BRANCH', out)
    else:
        d.setVar('LATEST_KERNEL_BRANCH', 'v5.10.y-dovetail')
}

# default: amd64 or i386
GIT_REPO = "${@ \
    'git://github.com/xenomai-ci/ipipe.git' if is_xeno_3_0(d) \
    else 'git://github.com/xenomai-ci/ipipe-x86.git' if is_kernel(d, '4.19') or is_kernel(d, '5.4') \
    else 'git://source.denx.de/xenomai/linux-dovetail.git' }"
GIT_BRANCH = "${@ \
    'ipipe-4.4.y-cip' if is_xeno_3_0(d) \
    else 'ipipe-x86-4.19.y-cip' if is_kernel(d, '4.19') \
    else 'ipipe-x86-5.4.y' if is_kernel(d, '5.4') \
    else 'v5.10.y-dovetail-rebase' if is_kernel(d, '5.10') \
    else 'v5.15.y-dovetail-rebase' if is_kernel(d, '5.15') \
    else 'v6.1.y-dovetail-rebase' if is_kernel(d, '6.1') \
    else 'v6.6.y-dovetail-rebase' if is_kernel(d, '6.6') \
    else d.getVar('LATEST_KERNEL_BRANCH') }"

GIT_REPO:armhf = "${@ \
    'git://github.com/xenomai-ci/ipipe.git' if is_xeno_3_0(d) \
    else 'git://github.com/xenomai-ci/ipipe-arm.git' if is_kernel(d, '4.19') or is_kernel(d, '5.4') \
    else 'git://source.denx.de/xenomai/linux-dovetail.git' }"
GIT_BRANCH:armhf = "${@ \
    'ipipe-4.4.y-cip' if is_xeno_3_0(d) \
    else 'ipipe/4.19.y-cip' if is_kernel(d, '4.19') \
    else 'ipipe/5.4.y' if is_kernel(d, '5.4') \
    else 'v5.10.y-dovetail-rebase' if is_kernel(d, '5.10') \
    else 'v5.15.y-dovetail-rebase' if is_kernel(d, '5.15') \
    else 'v6.1.y-dovetail-rebase' if is_kernel(d, '6.1') \
    else 'v6.6.y-dovetail-rebase' if is_kernel(d, '6.6') \
    else d.getVar('LATEST_KERNEL_BRANCH') }"

GIT_REPO:arm64 = "${@ \
    'git://github.com/xenomai-ci/ipipe-arm64.git' if is_kernel(d, '4.19') or is_kernel(d, '5.4') \
    else 'git://source.denx.de/xenomai/linux-dovetail.git' }"
GIT_BRANCH:arm64 = "${@ \
    'ipipe/4.19.y-cip' if is_kernel(d, '4.19') \
    else 'ipipe/5.4.y' if is_kernel(d, '5.4') \
    else 'v5.10.y-dovetail-rebase' if is_kernel(d, '5.10') \
    else 'v5.15.y-dovetail-rebase' if is_kernel(d, '5.15') \
    else 'v6.1.y-dovetail-rebase' if is_kernel(d, '6.1') \
    else 'v6.6.y-dovetail-rebase' if is_kernel(d, '6.6') \
    else d.getVar('LATEST_KERNEL_BRANCH') }"

SRC_URI += "${GIT_REPO};protocol=https;branch=${GIT_BRANCH}"
SRCREV = "${AUTOREV}"

SRC_URI:append:beagle-bone-black = " ${@ 'file://bbb_4.4.cfg' if is_xeno_3_0(d) else '' }"
