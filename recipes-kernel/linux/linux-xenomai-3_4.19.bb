#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2019-2023
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require recipes-kernel/linux/linux-xenomai-3.inc

SRC_URI:append:amd64 = " git://github.com/xenomai-ci/ipipe-x86.git;protocol=https;nobranch=1"
SRCREV:amd64 ?= "ipipe-core-4.19.317-cip111-x86-29"

SRC_URI:append:arm64 = " git://github.com/xenomai-ci/ipipe-arm64.git;protocol=https;nobranch=1"
SRCREV:arm64 ?= "ipipe-core-4.19.229-cip67-arm64-13"

SRC_URI:append:armhf = " git://github.com/xenomai-ci/ipipe-arm.git;protocol=https;nobranch=1"
SRCREV:armhf ?= "ipipe-core-4.19.229-cip67-arm-18"

PV ?= "${@d.getVar('SRCREV').split('-')[2]}+"
