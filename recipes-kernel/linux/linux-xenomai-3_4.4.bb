#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2018-2023
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require recipes-kernel/linux/linux-xenomai-3.inc

SRC_URI:append:amd64 = " git://github.com/xenomai-ci/ipipe.git;protocol=https;nobranch=1"
SRCREV:amd64 ?= "ipipe-core-4.4.302-cip79-x86-39"

SRC_URI:append:armhf = " git://github.com/xenomai-ci/ipipe.git;protocol=https;nobranch=1"
SRC_URI:append:armhf = " ${@ 'file://bbb_4.4.cfg' if d.getVar('MACHINE') == 'beagle-bone-black' else '' }"
SRCREV:armhf ?= "ipipe-core-4.4.302-cip79-arm-24"

PV ?= "${@d.getVar('SRCREV').split('-')[2]}+"
