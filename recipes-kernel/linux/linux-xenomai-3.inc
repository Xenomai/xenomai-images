#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2018-2023
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require recipes-kernel/linux/linux-xenomai.inc

SRC_URI += "file://rules.tmpl"

DEPENDS += "xenomai-kernel-source"
KBUILD_DEPENDS += "xenomai-kernel-source:native"

TEMPLATE_FILES += "rules.tmpl"

do_prepare_debian_rules() {
    mv ${WORKDIR}/debian/rules ${WORKDIR}/debian/rules.isar
    mv ${WORKDIR}/rules ${WORKDIR}/debian/rules
}
addtask prepare_debian_rules after do_transform_template before do_prepare_build
