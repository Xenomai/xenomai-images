#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2019-2023
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: MIT
#
device_type: qemu
job_name: QEMU pipeline, xenomai-images-qemu-armhf

timeouts:
  job:
    minutes: 90
  action:
    minutes: 65
  connection:
    minutes: 5
priority: medium
visibility: public

# context allows specific values to be overridden or included
context:
  arch: arm
  cpu: cortex-a15
  extra_options:
  - -smp 2
  - -device virtio-serial-device
  - -device virtconsole,chardev=con
  - -chardev vc,id=con
  - -device virtio-blk-device,bootindex=0,drive=disk
  - -device virtio-blk-device,bootindex=1,drive=lavatest
  guestfs_interface: none
metadata:
  # please change these fields when modifying this job for your own tests.
  docs-source: xenomai-qemuarmhf
  docs-filename: qemu-pipeline-xenomai-armhf

# ACTION_BLOCK
actions:
- deploy:
    timeout:
      minutes: 15
    to: tmpfs
    images:
      kernel:
        image_arg: -kernel {kernel} -append "root=/dev/vdb rw"
        url: ${DEPLOY_URL}/${ISAR_IMAGE}-${ISAR_DISTRIBUTION}-${TARGET}-vmlinuz
      initrd:
        image_arg: -initrd {initrd}
        url: ${DEPLOY_URL}/${ISAR_IMAGE}-${ISAR_DISTRIBUTION}-${TARGET}-initrd.img
      rootfs:
        image_arg: -drive file={rootfs},discard=unmap,if=none,id=disk,format=raw
        url: ${DEPLOY_URL}/${ISAR_IMAGE}-${ISAR_DISTRIBUTION}-${TARGET}.ext4.gz
        compression: gz
# BOOT_BLOCK
- boot:
    timeout:
      minutes: 5
    method: qemu
    media: tmpfs
    prompts: ["root@demo:"]
    auto_login:
      login_prompt: 'demo login:'
      username: root
      password_prompt: 'Password:'
      password: root

- test:
    timeout:
      minutes: ${TEST_TIMEOUT_MINUTES}
    definitions:
    - repository:
        metadata:
          format: Lava-Test Test Definition 1.0
          name: xenomai-test-suite
          description: "runs the xenomai test suite"
        run:
          steps:
            - ${TEST_COMMAND} || lava-test-raise "xenomai-test failed!"
      from: inline
      path: inline/xenomai-test-suite.yaml
      name: xenomai-test-suite
