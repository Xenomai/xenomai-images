#!/bin/sh

#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2019-2021
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: MIT
#
set -e
target="$1"

if [ -z "${target}" ]; then
    exit 1
fi
images_dir=build/tmp/deploy/images

destination="${S3_BUCKET_URL:-s3://xenomai-images-artifacts/artifacts}"

if [ -z "${destination}" ] || [ -z "${AWS_ACCESS_KEY_ID}" ]  || [ -z "${AWS_SECRET_ACCESS_KEY}" ]; then
    echo "environment not available or incomplete - do not deploy"
    exit 0
fi

isar_base_name="${ISAR_IMAGE}-${ISAR_DISTRIBUTION}-${target}"
deploy_dir="${destination}/${CI_PIPELINE_ID}/${BUILD_IDENTIFIER}"

kernel="${images_dir}/${target}/${isar_base_name}-vmlinuz"
kernel_suffix="vmlinuz"
if [ ! -f "${kernel}" ]; then
    # care about arm64 targets
    kernel="${images_dir}/${target}/${isar_base_name}-vmlinux"
    kernel_suffix="vmlinux"
fi

#KERNEL
aws s3 cp "${kernel}" "${deploy_dir}/${isar_base_name}-${kernel_suffix}"
# INITRD
aws s3 cp "${images_dir}/${target}/${isar_base_name}-initrd.img" "${deploy_dir}/${isar_base_name}-initrd.img"
# ROOTFS
if ! ls "${images_dir}/${target}/${isar_base_name}".*.gz ; then
    gzip "${images_dir}/${target}/${isar_base_name}.*"
fi
image=$(ls "${images_dir}/${target}/${isar_base_name}".*.gz)
aws s3 cp "${image}" "${deploy_dir}/$(basename "${image}")"
# DTB
dtb=$(ls "${images_dir}/${target}/"*.dtb 2> /dev/null || true )
if [ -n "${dtb}" ] ; then
    aws s3 cp  "${dtb}" "${deploy_dir}/$(basename "${dtb}")"
fi
