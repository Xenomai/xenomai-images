#!/bin/sh
#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2019-2023
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: MIT
#
set -e
TARGET=$1
if [ -z "${TARGET}" ]; then
    echo "no target was given"
    exit 1
fi

get_first_xml_attr_value()
{
	file=${1}
	tag=${2}

	grep -m 1 -o "${tag}=\".*\"" ${file} | cut -d\" -f2
}

# S3 artifacts
artifact_url="${LAVA_ARTIFACTS_URL:-https://d1a54g2tyy2hl.cloudfront.net/artifacts}"
# public master
lava_master_uri="${LAVA_MASTER_URL:-https://lava.xenomai.org}"

echo "connect to lava server: ${lava_master_uri}"

# connect to lava master
lavacli identities add --token "${LAVA_MASTER_TOKEN}" --uri "${lava_master_uri}" --username "${LAVA_MASTER_ACCOUNT}" default

#generate lava job description from template
DEPLOY_URL="${artifact_url}/${CI_PIPELINE_ID}/${BUILD_IDENTIFIER}"
export DEPLOY_URL

TEST_TIMEOUT_MINUTES=$((TEST_RUNTIME_SECONDS / 60 + ADDITIONAL_TEST_TIMEOUT_MINUTES))
export TEST_TIMEOUT_MINUTES

echo "Deploy artifacts from '${artifact_url}'"

tmp_dir=$(mktemp -d)
template="${tmp_dir}/job_${TARGET}_${CI_PIPELINE_ID}.yml"

cat "tests/jobs/xenomai-${TARGET}.yml" | envsubst > "$template"

echo "Submit lava job"

test_id=$(lavacli jobs submit "${template}")
echo "URL: ${lava_master_uri}/scheduler/job/${test_id}"

lavacli jobs logs "${test_id}"
lavacli results "${test_id}"

# add test report download
api="${lava_master_uri}/api/v0.2/jobs/${test_id}/junit"
auth="?token=${LAVA_MASTER_TOKEN}"
url="${api}/${auth}"

curl --silent "${url}" -o report.xml

# change return code to generate a error in gitlab-ci if a test is failed
if xmlstarlet sel -t  -v "/testsuites/testsuite/testcase[@name='job']/failure/@type" report.xml; then
	exit 1
fi
