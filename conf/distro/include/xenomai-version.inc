#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2019-2024
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: MIT
#

XENOMAI_DEFAULT_KERNEL_VERSION = "6.1%"

XENOMAI_3_0_VERSION_LIST = "3.0 3.0% 3.0.% 3.0.15 stable-3.0.x"
XENOMAI_3_0_KERNEL_VERSION = "4.4%"

XENOMAI_3_1_VERSION_LIST = "3.1 3.1% 3.1.% 3.1.5 stable-3.1.x"
XENOMAI_3_1_KERNEL_VERSION = "5.4%"

XENOMAI_3_2_VERSION_LIST = "3.2 3.2% 3.2.% 3.2.6 stable-3.2.x"
XENOMAI_3_2_KERNEL_VERSION = "6.1%"

# If PREFERRED_VERSION_xenomai contains any item from XENOMAI_3_0_VERSION_LIST use
# XENOMAI_3_0_KERNEL_VERSION. Same for the other XENOMAI_*_VERSION_LIST.
PREFERRED_VERSION_linux-xenomai-3 ?= \
    "${@bb.utils.contains_any('PREFERRED_VERSION_xenomai', \
        '${XENOMAI_3_0_VERSION_LIST}', '${XENOMAI_3_0_KERNEL_VERSION}', \
        bb.utils.contains_any('PREFERRED_VERSION_xenomai', \
            '${XENOMAI_3_1_VERSION_LIST}', '${XENOMAI_3_1_KERNEL_VERSION}', \
            bb.utils.contains_any('PREFERRED_VERSION_xenomai', \
                '${XENOMAI_3_2_VERSION_LIST}', '${XENOMAI_3_2_KERNEL_VERSION}', \
                '${XENOMAI_DEFAULT_KERNEL_VERSION}', d), \
            d), \
        d)}"
