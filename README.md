# Xenomai Demonstration and Test Images

This generates a number of bootable image for virtual and real targets in order
to run the [Xenomai](https://www.xenomai.org) real-time system, for evaluation
or testing purposes. The layer may also be used to build real applications on
top.

The build system used for this is [Isar](https://github.com/ilbers/isar), an
image generator that assembles Debian binaries or builds individual packages
from scratch.

## Building Target Images

Install docker and make sure you have required permissions to start containers.

Next invoke the configuration menu to select the desired image configuration
and start the build:

    ./kas-container menu

If you built, e.g., a QEMU AMD64 image, this can be run using
`start-qemu.sh x86`.

You can also select the desired image configuration on the command line.  To
build the QEMU AMD64 target with debug options enabled, invoke kas-container
like this:

    ./kas-container build kas.yml:board-qemu-amd64.yml:opt-debug.yml

### Physical Targets

This repository contains recipes for x86 (`board-x86-64-efi.yml`),
armhf(`board-beagle-bone-black.yml`) and arm64(`board-hikey.yml`) targets.

Each physical target will generate ready-to-boot images under
`build/tmp/deploy/images/`. To flash, e.g., the HiKey image to an SD card, run

    dd if=build/tmp/deploy/images/hikey/demo-image-hikey-xenomai-demo-hikey.wic \
       of=/dev/<medium-device> bs=1M status=progress

## CI Build

The CI build can be executed locally by
[installing the gitlab-runner](https://docs.gitlab.com/runner/install/).

To execute the CI build use:
```
gitlab-runner exec docker --docker-privileged \
  --env "HTTP_PROXY=$HTTP_PROXY" --env "HTTPS_PROXY=$HTTPS_PROXY" \
  --env "NO_PROXY=$NO_PROXY" build:qemu-armhf
```

## LAVA Lab

The [Xenomai project](https://www.xenomai.org/) maintains a
[LAVA lab](https://lava.xenomai.org) for integration testing.

A LAVA lab can be created by following the
[installation instructions](https://docs.lavasoftware.org/lava/first-installation.html)
or use
[lava docker](https://github.com/kernelci/lava-docker) for a faster setup.

It is also possible to connect a lab to lava.xenomai.org. Please get in
touch with the maintainers via mailing list to join a new lab with
lava.xenomai.org.

## Community Resources

See [Xenomai project](https://www.xenomai.org/).

Contributions are always welcome. They follow the same
[process](https://gitlab.denx.de/Xenomai/xenomai/blob/master/CONTRIBUTING.md)
that Xenomai is using as well. Please tag you patches additional with
"[xenomai-images]" to indicate which repository they are targeting.

## License

Unless otherwise stated in the respective file, files in this layer are
provided under the MIT license, see COPYING file. Patches (files ending with
.patch) are licensed according to their target project and file, typically
GPLv2.
